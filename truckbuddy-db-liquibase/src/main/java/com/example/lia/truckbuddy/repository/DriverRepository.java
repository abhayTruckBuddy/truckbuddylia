package com.example.lia.truckbuddy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.lia.truckbuddy.entity.Driver;

public interface DriverRepository extends JpaRepository<Driver, String> {

}
