package com.example.lia.truckbuddy.entity;

import java.util.Arrays;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bank_account_details")
public class BankAccountDetails {

	@Id
	@Column(name = "bank_account_number")
	private String bankAccountNumber;
	@Column(name = "bank_name", nullable = false)
	private String bankName;
	@Column(name = "ifsc_code", nullable = false)
	private String ifscCode;
	@Column(name = "account_holder_name", nullable = false)
	private String accountHolderName;
	@Column(name = "account_type", nullable = false)
	private String accountType;
	@Lob
	@Column(name = "cancelled_check", columnDefinition = "blob")
	private byte[] cancelledCheque;
//	@OneToOne(mappedBy = "bankAccountDetails", cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
//			CascadeType.REFRESH })
//	private Tap tap;

	public BankAccountDetails() {
		// TODO Auto-generated constructor stub
	}

	public BankAccountDetails(String bankAccountNumber, String bankName, String ifscCode, String accountHolderName,
			String accountType) {
		this.bankAccountNumber = bankAccountNumber;
		this.bankName = bankName;
		this.ifscCode = ifscCode;
		this.accountHolderName = accountHolderName;
		this.accountType = accountType;
	}

	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getAccountHolderName() {
		return accountHolderName;
	}

	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public byte[] getCancelledCheque() {
		return cancelledCheque;
	}

	public void setCancelledCheque(byte[] cancelledCheque) {
		this.cancelledCheque = cancelledCheque;
	}

//	public Tap getTap() {
//		return tap;
//	}
//
//	public void setTap(Tap tap) {
//		this.tap = tap;
//	}

	@Override
	public String toString() {
		return "BankAccountDetails [bankAccountNumber=" + bankAccountNumber + ", bankName=" + bankName + ", ifscCode="
				+ ifscCode + ", accountHolderName=" + accountHolderName + ", accountType=" + accountType
				+ ", cancelledCheque=" + Arrays.toString(cancelledCheque) +  "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bankAccountNumber == null) ? 0 : bankAccountNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BankAccountDetails other = (BankAccountDetails) obj;
		if (bankAccountNumber == null) {
			if (other.bankAccountNumber != null)
				return false;
		} else if (!bankAccountNumber.equals(other.bankAccountNumber))
			return false;
		return true;
	}

}
