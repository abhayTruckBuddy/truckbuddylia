package com.example.lia.truckbuddy.common;

public class RandomNumberGenerator {

	public static String getRandomAlphaNumeric(int length) {
		String chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String result = "";
		for (int i = length; i > 0; --i) {
			result += chars.charAt((int) Math.round(Math.random() * (chars.length() - 1)));
		}
		return result;
	}

	public static String getRandomNumeric(int length) {
		String chars = "0123456789";
		String result = "";
		for (int i = length; i > 0; --i) {
			result += chars.charAt((int) Math.round(Math.random() * (chars.length() - 1)));
		}
		return result;
	}

	public static String getRandomAlphabets(int length) {
		String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String result = "";
		for (int i = length; i > 0; --i) {
			result += chars.charAt((int) Math.round(Math.random() * (chars.length() - 1)));
		}
		return result;
	}

	public static String getRandomLowerAlphabets(int length) {
		String chars = "abcdefghijklmnopqrstuvwxyz";
		String result = "";
		for (int i = length; i > 0; --i) {
			result += chars.charAt((int) Math.round(Math.random() * (chars.length() - 1)));
		}
		return result;
	}

	public static String getRandomUpperAlphabets(int length) {
		String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String result = "";
		for (int i = length; i > 0; --i) {
			result += chars.charAt((int) Math.round(Math.random() * (chars.length() - 1)));
		}
		return result;
	}

}
