package com.example.lia.truckbuddy.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.lia.truckbuddy.entity.Driver;
import com.example.lia.truckbuddy.repository.DriverRepository;

@Service
public class DriverServiceImpl implements DriverService {

	@Autowired
	private DriverRepository driverRepository;

	@Override
	public void saveDriver(Driver driver) {
		driverRepository.save(driver);

	}

	@Override
	public Driver getDriver(String driverId) {
		Optional<Driver> result = driverRepository.findById(driverId);

		if (result.isPresent()) {
			return result.get();
		} else {
			throw new RuntimeException("Driver not found with id : " + driverId);
		}
	}

	@Override
	public List<Driver> getDrivers() {
		return driverRepository.findAll();
	}

}
