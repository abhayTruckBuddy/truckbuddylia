package com.example.lia.truckbuddy.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.data.domain.Persistable;

@Entity
@Table(name = "tap")
public class Tap implements Persistable<String> {

	@Id
	@Column(name = "mobile")
	private String mobile;
	@Column(name = "first_name", nullable = false)
	private String firstName;
	@Column(name = "last_name", nullable = false)
	private String lastName;
	@Column(name = "email", unique = true)
	private String email;
	@Column(name = "alternate_mobile")
	private String alternateMobile;
	@Column(name = "address", nullable = false)
	private String address;
	@Column(name = "city", nullable = false)
	private String city;
	@Column(name = "state", nullable = false)
	private String state;
	@Column(name = "gst_number", unique = true, nullable = false)
	private String gstNumber;
	@Column(name = "company_name", nullable = false)
	private String companyName;
	@Column(name = "pan_card", unique = true, nullable = false)
	private String panCard;
	@Column(name = "aadhar_card", unique = true, nullable = false)
	private String aadharCard;
	@Column(name = "password", nullable = false)
	private String password;
	@Column(name = "status", nullable = false, columnDefinition = "tinyint(1) default 1")
	private boolean status = true;
	@Column(name = "password_reset", nullable = false, columnDefinition = "tinyint(1) default 0")
	private boolean passwordReset;
//	@OneToMany(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
	@Column(name = "load_type_id", nullable = false)
	private int loadTypeId;
//	private Set<LoadType> loadTypes = new HashSet<LoadType>();
	@Lob
	@Column(name = "profile_pic", columnDefinition = "BLOB")
	private byte[] profilePic;
	@Lob
	@Column(name = "agreement_copy", columnDefinition = "BLOB")
	private byte[] agreementCopy;
	@Lob
	@Column(name = "gst_certificate", columnDefinition = "BLOB")
	private byte[] gstCertificate;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "bank_account_details_id")
	private BankAccountDetails bankAccountDetails;

//	@OneToMany(mappedBy = "tap", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//	private Set<Driver> drivers = new HashSet<Driver>();
//	@OneToMany(mappedBy = "tap", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//	private Set<Vehicle> vehicles = new HashSet<Vehicle>();

	private @Transient boolean isNew = true;

	public Tap() {
		// TODO Auto-generated constructor stub
	}

	public Tap(String mobile, String firstName, String lastName, String email, String alternateMobile, String address,
			String city, String state, String gstNumber, String companyName, String panCard, String aadharCard,
			String password, boolean status, boolean passwordReset) {
		this.mobile = mobile;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.alternateMobile = alternateMobile;
		this.address = address;
		this.city = city;
		this.state = state;
		this.gstNumber = gstNumber;
		this.companyName = companyName;
		this.panCard = panCard;
		this.aadharCard = aadharCard;
		this.password = password;
		this.status = status;
		this.passwordReset = passwordReset;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAlternateMobile() {
		return alternateMobile;
	}

	public void setAlternateMobile(String alternateMobile) {
		this.alternateMobile = alternateMobile;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getGstNumber() {
		return gstNumber;
	}

	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getPanCard() {
		return panCard;
	}

	public void setPanCard(String panCard) {
		this.panCard = panCard;
	}

	public String getAadharCard() {
		return aadharCard;
	}

	public void setAadharCard(String aadharCard) {
		this.aadharCard = aadharCard;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public boolean isPasswordReset() {
		return passwordReset;
	}

	public void setPasswordReset(boolean passwordReset) {
		this.passwordReset = passwordReset;
	}

	public int getLoadTypeId() {
		return loadTypeId;
	}

	public void setLoadTypeId(int loadTypeId) {
		this.loadTypeId = loadTypeId;
	}

	public byte[] getProfilePic() {
		return profilePic;
	}

	public void setProfilePic(byte[] profilePic) {
		this.profilePic = profilePic;
	}

	public byte[] getAgreementCopy() {
		return agreementCopy;
	}

	public void setAgreementCopy(byte[] agreementCopy) {
		this.agreementCopy = agreementCopy;
	}

	public byte[] getGstCertificate() {
		return gstCertificate;
	}

	public void setGstCertificate(byte[] gstCertificate) {
		this.gstCertificate = gstCertificate;
	}

	public BankAccountDetails getBankAccountDetails() {
		return bankAccountDetails;
	}

	public void setBankAccountDetails(BankAccountDetails bankAccountDetails) {
		this.bankAccountDetails = bankAccountDetails;
	}

//	public Set<Driver> getDrivers() {
//		return drivers;
//	}
//
//	public void setDrivers(Set<Driver> drivers) {
//		this.drivers = drivers;
//	}
//
//	public Set<Vehicle> getVehicles() {
//		return vehicles;
//	}
//
//	public void setVehicles(Set<Vehicle> vehicles) {
//		this.vehicles = vehicles;
//	}

	@Override
	public String toString() {
		return "Tap [mobile=" + mobile + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
				+ ", alternateMobile=" + alternateMobile + ", address=" + address + ", city=" + city + ", state="
				+ state + ", gstNumber=" + gstNumber + ", companyName=" + companyName + ", panCard=" + panCard
				+ ", aadharCard=" + aadharCard + ", password=" + password + ", status=" + status + ", passwordReset="
				+ passwordReset + ", loadTypeId=" + loadTypeId + ", bankAccountDetails=" + bankAccountDetails + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mobile == null) ? 0 : mobile.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tap other = (Tap) obj;
		if (mobile == null) {
			if (other.mobile != null)
				return false;
		} else if (!mobile.equals(other.mobile))
			return false;
		return true;
	}

	@Override
	public String getId() {
		return mobile;
	}

	@PrePersist
	@PostLoad
	void markNotNew() {
		this.isNew = false;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

	@Override
	public boolean isNew() {
		return isNew;
	}

}
