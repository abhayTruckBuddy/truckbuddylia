package com.example.lia.truckbuddy.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.lia.truckbuddy.common.RandomNumberGenerator;
import com.example.lia.truckbuddy.entity.Tap;
import com.example.lia.truckbuddy.repository.TapRepository;

@Service
public class TapServiceImpl implements TapService {

	@Autowired
	private TapRepository tapRepository;

	@Override
	@Transactional
	public List<Tap> getTaps() {
		return tapRepository.findAll();
	}

	@Override
	@Transactional
	public Tap getTap(String id) {
		Optional<Tap> result = tapRepository.findById(id);

		if (result.isPresent()) {
			return result.get();
		} else {
			throw new RuntimeException("Tap not found with id : " + id);
		}
	}

	@Override
	@Transactional
	public void saveTap(Tap tap) {
		if (tap.isNew()) {
			tap.setPassword(RandomNumberGenerator.getRandomAlphaNumeric(8));
		}
		tapRepository.save(tap);

	}

}
