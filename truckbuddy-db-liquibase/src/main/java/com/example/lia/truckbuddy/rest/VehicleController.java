package com.example.lia.truckbuddy.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.lia.truckbuddy.entity.Vehicle;
import com.example.lia.truckbuddy.service.VehicleService;

@RestController
@RequestMapping("/truckbuddy/lia/api/tap")
public class VehicleController {

	@Autowired
	private VehicleService vehicleService;

	@PostMapping("/addvehicle")
	public Vehicle createTap(@RequestBody Vehicle vehicle) {

		vehicleService.saveVehicle(vehicle);
		return vehicle;
	}

	@PutMapping("/updatevehicle/{vehicleNumber}")
	public Vehicle updateDriver(@PathVariable String vehicleNumber, @RequestBody Vehicle vehicleBody) {
		Vehicle vehicle = vehicleService.getVehicle(vehicleNumber);
		if (vehicle != null) {
			vehicleBody.setNew(false);
			vehicleService.saveVehicle(vehicleBody);
		} else {
			throw new RuntimeException("Vehicle not found with Vehicle Number : " + vehicleNumber);
		}
		return vehicleBody;
	}

	@GetMapping("/vehicles")
	public List<Vehicle> getVehicles() {
		List<Vehicle> vehicles = vehicleService.getVehicle();
		return vehicles;
	}

	@GetMapping("/vehicles/{vehicleNumber}")
	public Vehicle getVehicle(@PathVariable String vehicleNumber) {
		Vehicle vehicle = vehicleService.getVehicle(vehicleNumber);
		if (vehicle != null) {
			return vehicle;
		} else {
			throw new RuntimeException("Driver not found with id : " + vehicleNumber);
		}
	}

}
