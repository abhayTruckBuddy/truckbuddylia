package com.example.lia.truckbuddy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.lia.truckbuddy.entity.Vehicle;

public interface VehicleRepository extends JpaRepository<Vehicle, String> {

}
