package com.example.lia.truckbuddy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TruckbuddyApplication {

	public static void main(String[] args) {
		SpringApplication.run(TruckbuddyApplication.class, args);
	}

}
