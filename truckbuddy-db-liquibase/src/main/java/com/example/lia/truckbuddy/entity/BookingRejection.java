package com.example.lia.truckbuddy.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "booking_rejection")
public class BookingRejection {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "booking_rejection_id")
	private int id;
	@Column(name = "rejection_criteria", nullable = false, unique = true)
	private String rejectionCriteria;
	@Column(name = "remarks")
	private String remarks;

	public BookingRejection() {
		// TODO Auto-generated constructor stub
	}

	public BookingRejection(String rejectionCriteria, String remarks) {
		this.rejectionCriteria = rejectionCriteria;
		this.remarks = remarks;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRejectionCriteria() {
		return rejectionCriteria;
	}

	public void setRejectionCriteria(String rejectionCriteria) {
		this.rejectionCriteria = rejectionCriteria;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		return "BookingRejection [id=" + id + ", rejectionCriteria=" + rejectionCriteria + ", remarks=" + remarks + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BookingRejection other = (BookingRejection) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
