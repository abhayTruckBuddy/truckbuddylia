package com.example.lia.truckbuddy.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "load_type")
public class LoadType {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "load_type_id")
	private int id;
	@Column(name = "load_type_name", nullable = false, unique = true)
	private String loadTypeName;
	@Column(name = "description")
	private String description;

	public LoadType() {
		// TODO Auto-generated constructor stub
	}

	public LoadType(String loadTypeName, String description) {
		this.loadTypeName = loadTypeName;
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLoadTypeName() {
		return loadTypeName;
	}

	public void setLoadTypeName(String loadTypeName) {
		this.loadTypeName = loadTypeName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "LoadType [id=" + id + ", loadTypeName=" + loadTypeName + ", description=" + description + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LoadType other = (LoadType) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
