package com.example.lia.truckbuddy.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "booking_response")
public class BookingResponse {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "booking_response_id")
	private long id;
	@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH }, targetEntity = BookingRequest.class)
	@JoinColumn(name = "booking_request_id", nullable = false)
	private BookingRequest bookingRequest;
	@Column(name = "vehicle_id")
	private String vehicleId;
	@Column(name = "driver_id")
	private String driverId;
	@OneToOne(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH }, targetEntity = BookingRejection.class)
	@JoinColumn(name = "booking_rejection_id")
	private BookingRejection bookingRejection;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "source_arrival_time")
	private Date sourceArrivalTime;

	public BookingResponse() {
		// TODO Auto-generated constructor stub
	}

	public BookingResponse(String vehicleId, String driverId, Date sourceArrivalTime) {
		this.vehicleId = vehicleId;
		this.driverId = driverId;
		this.sourceArrivalTime = sourceArrivalTime;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public BookingRequest getBookingRequest() {
		return bookingRequest;
	}

	public void setBookingRequest(BookingRequest bookingRequest) {
		this.bookingRequest = bookingRequest;
	}

	public String getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}

	public String getDriverId() {
		return driverId;
	}

	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}

	public BookingRejection getBookingRejection() {
		return bookingRejection;
	}

	public void setBookingRejection(BookingRejection bookingRejection) {
		this.bookingRejection = bookingRejection;
	}

	public Date getSourceArrivalTime() {
		return sourceArrivalTime;
	}

	public void setSourceArrivalTime(Date sourceArrivalTime) {
		this.sourceArrivalTime = sourceArrivalTime;
	}

	@Override
	public String toString() {
		return "BookingResponse [id=" + id + ", bookingRequest=" + bookingRequest + ", vehicleId=" + vehicleId
				+ ", driverId=" + driverId + ", bookingRejection=" + bookingRejection + ", sourceArrivalTime="
				+ sourceArrivalTime + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BookingResponse other = (BookingResponse) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
