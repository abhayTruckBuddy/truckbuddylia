package com.example.lia.truckbuddy.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DefaultController {

	// mapping for "/"
	@GetMapping("/")
	public String sayHello() {
		return "Spring Boot App running fine";
	}

}
