package com.example.lia.truckbuddy.service;

import java.util.List;

import com.example.lia.truckbuddy.entity.Tap;

public interface TapService {

	public List<Tap> getTaps();

	public Tap getTap(String id);

	public void saveTap(Tap tap);

}
