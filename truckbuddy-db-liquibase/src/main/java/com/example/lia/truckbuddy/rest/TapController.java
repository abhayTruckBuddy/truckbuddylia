package com.example.lia.truckbuddy.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.lia.truckbuddy.entity.Tap;
import com.example.lia.truckbuddy.service.TapService;

@RestController
@RequestMapping("/truckbuddy/lia/api/tap")
public class TapController {

	@Autowired
	private TapService tapService;

	@PostMapping("/create")
	public Tap createTap(@RequestBody Tap tap) {
		tapService.saveTap(tap);
		return tap;
	}

	@PutMapping("/update/{tapId}")
	public Tap updateTap(@PathVariable String tapId, @RequestBody Tap tapBody) {
		Tap tap = tapService.getTap(tapId);
		if (tap != null) {
			tapBody.setNew(false);
			tapService.saveTap(tapBody);
		} else {
			throw new RuntimeException("Tap not found with id : " + tapId);
		}
		return tapBody;
	}

	@GetMapping("/taps")
	public List<Tap> getTaps() {
		List<Tap> taps = tapService.getTaps();
		return taps;
	}

	@GetMapping("/taps/{tapId}")
	public Tap getTap(@PathVariable String tapId) {
		Tap tap = tapService.getTap(tapId);
		if (tap != null) {
			return tap;
		} else {
			throw new RuntimeException("Tap not found with id : " + tapId);
		}
	}
}
