<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="springForm"
	uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/DataTables/DataTables-1.10.21/css/dataTables.bootstrap4.min.css">

<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">

<title>Vehicle Driver Mapping</title>

<style type="text/css">
.errors {
	color: red;
}

.form-group.required .control-label:after {
	content: "*";
	color: red;
}

form .error {
	color: #ff0000;
}
</style>

</head>
<body>

	<div class="jumbotron text-center">
		<h2>Vehicle Driver Mapping</h2>
	</div>
	<div class="container">
		<div style="" class="text-right">
			<button type="button" class="btn btn-secondary"
				onclick="window.location = '${pageContext.request.contextPath}/'"
				class="btn btn-link">Home Page</button>
		</div>
		<div id="errorCardBody" name="errorCardBody" class="errors"></div>
		<c:if test="${formError}">
			<div class="alert alert-danger col-xs-offset-1 col-xs-10">
				Error while mapping. Please try again.</div>
		</c:if>

		<springForm:form name="tap-registraion-form"
			action="addVehicleDriverMapping" id="addVehicleDriverMapping"
			method="POST">

			<h4 class="card-title text-center">Vehicle Driver Mapping</h4>
			<section class="card-header">

				<div class="form-row form-group required">
					<div class="col-md-6 mb-3">
						<label for="vehicleNumber" class="control-label">Vehicle :</label>
						<select class="form-control" name="vehicleNumber"
							id="vehicleNumber">
							<option value="" label="--Select Vehicle--"></option>
							<c:forEach var="vehicle" items="${vehicleList}">
								<c:if test="${empty vehicle.driver.mobile}">
									<option value="${vehicle.vehicleNumber}"
										label="${vehicle.vehicleNumber}"></option>
								</c:if>
							</c:forEach>

						</select>
					</div>
					<div class="col-md-6 mb-3">
						<label for="driverId" class="control-label">Driver :</label> <select
							class="form-control" name="driverId" id="driverId">
							<option value="" label="--Select Driver"></option>
							<c:forEach var="driver" items="${driverList}">
								<c:if test="${empty driver.assignedVehicleNumber.vehicleNumber}">
									<option value="${driver.mobile}"
										label="${driver.firstName} ${driver.lastName}"></option>
								</c:if>
							</c:forEach>

						</select>
					</div>
				</div>

			</section>
			<br>
			<div class="container">
				<div class="row">
					<div class="col text-center">
						<button type="submit" id="submit" class="btn btn-primary">Submit</button>
					</div>
				</div>
			</div>
		</springForm:form>
		<hr class="my-4">
		<div id="main-div">
			<div class="page-header text-center">
				<h4 class="card-title">Driver Vehicle Mapping List</h4>
			</div>
			<section>
				<table id="tap-table" class="table table-striped table-bordered"
					style="width: 100%">
					<thead>
						<tr>
							<th>Driver Name</th>
							<th>Mobile</th>
							<th>Assigned Vehicle</th>
							<th>Action</th>
						</tr>
					</thead>

					<tbody>
						<c:forEach var="driver" items="${driverList}">
							<c:url var="viewDetails"
								value="/truckbuddy/lia/mvc/tap/disassociateVehicleDriver">
								<c:param name="mobile" value="${driver.mobile}" />
							</c:url>
							<c:if
								test="${not empty driver.assignedVehicleNumber.vehicleNumber}">
								<tr>
									<td>${driver.firstName}&nbsp;${driver.lastName}</td>
									<td>${driver.mobile}</td>
									<td>${driver.assignedVehicleNumber.vehicleNumber}</td>
									<td><a href="${viewDetails}">Disassociate</a></td>
								</tr>
							</c:if>
						</c:forEach>
					</tbody>

				</table>
			</section>
		</div>
	</div>

	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/jquery/jquery-3.5.1.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/jquery-validate-1.19.1/dist/jquery.validate.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/jquery-validate-1.19.1/dist/additional-methods.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/DataTables/DataTables-1.10.21/js/dataTables.bootstrap4.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/DataTables/DataTables-1.10.21/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tap-table').DataTable();
		});
		$(document).ready(function() {
			$('div:empty').remove();
		});
		$(function() {
			// Initialize form validation on the registration form.
			// It has the name attribute "registration"
			$("form[id='addVehicleDriverMapping']").validate({
				rules : {
					vehicleNumber : "required",
					driverId : "required"
				},
				// Specify validation error messages
				messages : {
					vehicleNumber : "Please select a vehicle",
					driverId : "Please select a driver",
				},
				submitHandler : function(form) {
					form.submit();
				}
			});

		});
	</script>


</body>
</html>