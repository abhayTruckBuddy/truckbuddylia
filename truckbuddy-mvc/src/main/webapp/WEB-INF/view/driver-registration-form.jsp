<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="springForm"
	uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<title>Driver Registration</title>


<script>
	$(document).ready(function() {
		$('div:empty').remove();
	});
</script>

<style type="text/css">
.errors {
	color: red;
}

.form-group.required .control-label:after {
	content: "*";
	color: red;
}

form .error {
	color: #ff0000;
}
</style>

</head>
<body>

	<div class="jumbotron text-center">
		<h2>Driver Registration Details:</h2>
	</div>
	<div class="container">

		<div style="" class="text-right">
			<button type="button" class="btn btn-secondary"
				onclick="window.location = '${pageContext.request.contextPath}/'"
				class="btn btn-link">Home Page</button>
		</div>
		<div id="errorCardBody" name="errorCardBody" class="errors"></div>
		<c:if test="${uploadError}">
			<div class="alert alert-danger col-xs-offset-1 col-xs-10">
				Error while upload File(s). Please try again.</div>
		</c:if>

		<div id="driverFormDiv">
			<springForm:form name="driver-registraion-form" action="addDriver"
				method="POST" id="addDriver" modelAttribute="driver"
				enctype="multipart/form-data">
				<h4 class="card-title">Basic Details</h4>
				<section class="card-header">
					<div class="form-row form-group required">
						<div class="col-md-6 mb-3">
							<label for="firstName" class="control-label">First Name :</label>
							<springForm:errors path="firstName" class="errors" />
							<springForm:input path="firstName" type="text"
								class="form-control" id="firstName"
								placeholder="Enter First Name" name="firstName" />
						</div>

						<div class="col-md-6 mb-3">
							<label for="lastName" class="control-label">Last Name :</label>
							<springForm:errors path="lastName" class="errors" />
							<springForm:input path="lastName" type="text"
								class="form-control" id="lastName" placeholder="Enter Last Name"
								name="lastName" />
						</div>
					</div>

					<div class="form-row form-group required">
						<div class="col-md-6 mb-3">
							<label for="mobile" class="control-label">Mobile Number :</label>
							<springForm:errors path="mobile" class="errors" />
							<springForm:input type="text" class="form-control" id="mobile"
								path="mobile" placeholder="Enter Mobile Number" name="mobile" />
						</div>
						<div class="col-md-6 mb-3">
							<label for="email">Email:</label>
							<springForm:errors path="email" class="errors" />
							<springForm:input type="text" class="form-control" id="email"
								path="email" placeholder="Enter Email" name="email" />
						</div>
					</div>


					<div class="form-row form-group required">
						<div class="col-md-6 mb-3">
							<label for="drivingLicenceNumber" class="control-label">Driving
								Licence Number</label>
							<springForm:errors path="drivingLicenceNumber" class="errors" />
							<springForm:input type="text" class="form-control"
								id="drivingLicenceNumber" path="drivingLicenceNumber"
								placeholder="Enter Driving Licence Number"
								name="drivingLicenceNumber" />
						</div>
						<div class="col-md-6 mb-3">
							<label for="aadharCardNumber" class="control-label">Aadhar
								Card Number :</label>
							<springForm:errors path="aadharCardNumber" class="errors" />
							<springForm:input type="text" class="form-control"
								id="aadharCardNumber" path="aadharCardNumber"
								placeholder="Enter Aadhar Number" name="aadharCardNumber" />
						</div>
					</div>


					<div class="form-group" class="col-sm-4 mb-3">
						<label for="vehicleNumber" class="control-label">Assign
							Vehicle :</label> <select class="form-control" name="vehicleNumber"
							id="vehicleNumber">
							<option value="" label="--Select Vehicle--"></option>
							<c:forEach var="vehicle" items="${vehicleList}">
								<c:if test="${empty vehicle.driver.mobile}">
									<option value="${vehicle.vehicleNumber}"
										label="${vehicle.vehicleNumber}"></option>
								</c:if>
							</c:forEach>

						</select>
					</div>
		</div>
		</section>
		<hr class="my-4">
		<h4 class="card-title">Documents Upload</h4>
		<section class="card-header">

			<div class="form-group required" class="col-sm-4 mb-3">
				<p class="control-label">Driving Licence :</p>
				<div class="custom-file mb-3">
					<input type="file" class="custom-file-input" id="file1"
						name="file1" /> <label class="custom-file-label" for="file1">Choose
						file</label>
				</div>
			</div>

			<div class="form-group required" class="col-sm-4 mb-3">
				<p class="control-label">Aadhar Card :</p>
				<div class="custom-file mb-3">
					<input type="file" class="custom-file-input" id="file2"
						name="file2" /> <label class="custom-file-label" for="file2">Choose
						file</label>
				</div>
			</div>
		</section>
		<br>
		<div class="container">
			<div class="row">
				<div class="col text-center">
					<button type="submit" id="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</div>
		</springForm:form>

	</div>



	</div>

	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/jquery/jquery-3.5.1.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/jquery-validate-1.19.1/dist/jquery.validate.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/jquery-validate-1.19.1/dist/additional-methods.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/tap-form-validation.js"></script>
	<script type="text/javascript">
		$(function() {
			// Initialize form validation on the registration form.
			// It has the name attribute "registration"
			$("form[id='addDriver']")
					.validate(
							{
								rules : {
									firstName : {
										required : true,
										lettersonly : true
									},
									lastName : {
										required : true,
										lettersonly : true
									},
									mobile : {
										required : true,
										number : true,
										minlength : 10,
										maxlength : 10,
										digits : true,
									},
									email : {
										email : true
									},
									drivingLicenceNumber : "required",
									aadharCardNumber : {
										required : true,
										number : true,
										minlength : 16,
										maxlength : 16,
										digits : true,
									}
								},
								// Specify validation error messages
								messages : {
									firstName : "Please enter your first name",
									lastName : "Please enter your last name",
									mobile : {
										required : "Please enter your mobile number",
										minlength : "Invalid mobile number",
										maxlength : "Invalid mobile number",
										number : "Invalid mobile number",
										digits : "Invalid number or character",
									},
									email : "Please enter a valid email address",
									aadharCardNumber : {
										required : "Please enter your Aadhar card Number",
										number : "Invalid Aadhar card number",
										minlength : "Invalid Aadhar card number",
										maxlength : "Invalid Aadhar card number",
										digits : "Invalid number or character",
									},
									drivingLicenceNumber : "Please enter Driving Licence Number",
								},
								submitHandler : function(form) {
									form.submit();
								}
							});

		});

		$.validator.addMethod('filesize', function(value, element, param) {
			return this.optional(element) || (element.files[0].size <= param)
		}, 'File size must be less than {0}');

		$.validator.addMethod("lettersonly", function(value, element) {
			console.log(value);
			return this.optional(element) || /^[A-Z,a-z," ""]+$/i.test(value);
		}, "Invalid character(s). Only Letters are allowed");

		$(function() {
			//			$("form[id='createTap']").validate();
			$("[name^=file]")
					.each(
							function() {
								$(this)
										.rules(
												"add",
												{
													required : true,
													extension : "jpg|jpeg|png",
													filesize : 1000000,
													messages : {
														required : "Please upload required files",
														extension : "Please upload files in jpg, jpeg or png format"
													}
												});
							});
		});
		// Add the following code if you want the name of the file appear on select
		$(".custom-file-input").on(
				"change",
				function() {
					var fileName = $(this).val().split("\\").pop();
					$(this).siblings(".custom-file-label").addClass("selected")
							.html(fileName);
				});
	</script>

</body>
</html>