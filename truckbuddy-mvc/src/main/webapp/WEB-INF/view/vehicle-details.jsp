<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="springForm"
	uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/bootstrap 4.5.0/css/bootstrap.min.css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/ajax/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/ajax/popper.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/bootstrap 4.5.0/js/bootstrap.min.js"></script>
<script>
	$(document).ready(function() {
		$('div:empty').remove();
	});
</script>
<title>Vehicle Detail</title>

<style type="text/css">
.errors {
	color: red;
}

.form-group.required .control-label:after {
	content: "*";
	color: red;
}

form .error {
	color: #ff0000;
}
</style>
</head>
<body>
	<div id="main-div">

		<div class="jumbotron text-center">
			<h2>Vehicle Details</h2>
		</div>

		<div id="container">
			<section class="card-header">
				<div style="" class="text-right">
					<button type="button" class="btn btn-secondary"
						onclick="window.location = '${pageContext.request.contextPath}/truckbuddy/lia/mvc/tap/showVehicleList'"
						class="btn btn-link">Vehicle List</button>
				</div>

				<springForm:form action="getVehicleList" modelAttribute="vehicle">

					<springForm:hidden path="vehicleNumber" />

					<div class="form-row form-group">
						<div class="col-md-6 mb-3">
							<label for="vehicleNumber" class="control-label">Vehicle
								Number:</label>
							<springForm:input type="text" id="vehicleNumber"
								class="form-control" path="vehicleNumber" name="vehicleNumber" />
						</div>
						<div class="col-md-6 mb-3">
							<label for="vehicleTypes" class="control-label">Vehicle
								Type:</label>
							<springForm:input type="text" id="vehicleTypes"
								class="form-control" path="vehicleTypes" name="vehicleTypes" />
						</div>
					</div>

					<div class="form-row form-group">
						<div class="col-md-6 mb-3">
							<label for="insuranceExpiryDate" class="control-label">Insurance
								Expiry Date</label>
							<springForm:input type="text" id="mobile" class="form-control"
								path="insuranceExpiryDate" name="insuranceExpiryDate" />
						</div>
						<div class="col-md-6 mb-3">
							<label for="permitExpiryDate" class="control-label">Permission
								Expiry Date</label>
							<springForm:input type="text" id="permitExpiryDate"
								class="form-control" path="permitExpiryDate"
								name="permitExpiryDate" />
						</div>
					</div>

					 <div class="form-row center form-group">
					 <div class="col-md-4 mb-3">
						</div>
						<div class="col-md-4 mb-4 border  border-top-0 text-center">
						<h5>Registration Certificate</h5>
							<img class="rounded img-thumbnail img-fluid"
								src="${pageContext.request.contextPath}/myImage/showVehicleRegistrationCertificate?id=${vehicle.vehicleNumber}&&imageType=registrationCertificate" />
						</div>
						<div class="col-md-4 mb-3">
						</div>
					</div> 

				</springForm:form>
			</section>
		</div>
	</div>
</body>
</html>