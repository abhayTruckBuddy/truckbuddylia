<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Vehicle List</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/DataTables/DataTables-1.10.21/css/dataTables.bootstrap4.min.css">

<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>

<script
	src="${pageContext.request.contextPath}/DataTables/DataTables-1.10.21/js/dataTables.bootstrap4.min.js"></script>


<script type="text/javascript"
	src="${pageContext.request.contextPath}/DataTables/DataTables-1.10.21/js/jquery.dataTables.min.js"></script>


<script>
	$(document).ready(function() {
		$('div:empty').remove();
	});
</script>


<style type="text/css">
.errors {
	color: red;
}

.form-group.required .control-label:after {
	content: "*";
	color: red;
}

form .error {
	color: #ff0000;
}
</style>

</head>
<body>
	<div id="main-div">
		<div class="jumbotron text-center">
			<h2>TAP List</h2>
		</div>

		<div id="tapListDiv">


			<section class="card-header">

				<div style="" class="text-right">
					<button type="button" class="btn btn-secondary"
						onclick="window.location = '${pageContext.request.contextPath}/'"
						class="btn btn-link">Home Page</button>
				</div>
				<table id="tap-table" class="table table-striped table-bordered"
					style="width: 100%">


					<thead>
						<tr>
							<th>Vehicle</th>
							<th>Vehicle Types</th>
							<th>Insurance Expiry date</th>
							<th>Permit Expiry date</th>
							<th>View Details</th>
						</tr>
					</thead>

					<tbody>
						<c:forEach var="vehicle" items="${vehicleList}">

							<c:url var="viewDetails"
								value="/truckbuddy/lia/mvc/tap/showVehicleDetails">
								<c:param name="vehicleNumber" value="${vehicle.vehicleNumber}" />
							</c:url>

							<tr>
								<td>${vehicle.vehicleNumber}</td>
								<td>${vehicle.vehicleTypes}</td>
								<td>${vehicle.insuranceExpiryDate}</td>
								<td>${vehicle.permitExpiryDate}</td>
								<td><a href="${viewDetails}">View Details</a></td>
							</tr>
						</c:forEach>
					</tbody>

				</table>

				<div style="" class="text-right">
					<button type="button" class="btn btn-secondary"
						onclick="window.location = '${pageContext.request.contextPath}/'"
						class="btn btn-link">Home Page</button>
				</div>

			</section>
		</div>
	</div>

	<!-- ----------------------------Script Tag---------------------------- -->
	<script>
		$(document).ready(function() {
			$('#tap-table').DataTable();
		});
	</script>

</body>
</html>