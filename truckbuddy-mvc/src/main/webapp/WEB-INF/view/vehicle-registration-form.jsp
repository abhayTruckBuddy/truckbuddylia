<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="springForm"
	uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

<script type="text/javascript"
	src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript"
	src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<link rel="stylesheet"
	href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" />


<title>Vehicle Registration</title>


<script>
	$(document).ready(function() {
		$('div:empty').remove();
	});
</script>

<style type="text/css">
.errors {
	color: red;
}

.form-group.required .control-label:after {
	content: "*";
	color: red;
}

form .error {
	color: #ff0000;
}
</style>
</head>
<body>
	<div class="jumbotron text-center">
		<h2>Vehicle Registration Details:</h2>
	</div>
	<div class="container">

		<div style="" class="text-right">
			<button type="button" class="btn btn-secondary"
				onclick="window.location = '${pageContext.request.contextPath}/'"
				class="btn btn-link">Home Page</button>
		</div>
		<div id="vehicleFormDiv">
			<c:if test="${uploadError}">
				<div class="alert alert-danger col-xs-offset-1 col-xs-10">
					Error while upload File(s). Please try again.</div>
			</c:if>
			<springForm:form name="vehicleForm" action="addVehicle" method="POST"
				id="addVehicle" modelAttribute="vehicle"
				enctype="multipart/form-data">
				<h4 class="card-title">Basic Details</h4>
				<section class="card-header">
					<div class="form-row form-group required">
						<div class="col-md-6 mb-3">
							<label for="vehicleNumber" class="control-label">Vehicle
								Number :</label>
							<springForm:errors path="vehicleNumber" class="errors" />
							<springForm:input type="text" class="form-control"
								path="vehicleNumber" id="vehicleNumber"
								placeholder="Enter Vehicle Number" name="vehicleNumber" />
						</div>
						<div class="col-md-6 mb-3">
							<label for="vehicleTypes" class="control-label">Vehicle
								Type :</label>
							<springForm:errors path="vehicleTypes" class="errors" />
							<springForm:input path="vehicleTypes" type="text"
								class="form-control" id="vehicleTypes"
								placeholder="Enter Vehicle Type" name="vehicleTypes" />
						</div>
					</div>
					<div class="form-row form-group required">
						<div class="col-md-6 mb-3">
							<!-- Date input -->
							<label class="control-label" for="date">Insurance Expiry
								Date</label>
							<springForm:errors path="insuranceExpiryDate" class="errors" />
							<springForm:input class="form-control" id="insuranceExpiryDate"
								name="insuranceExpiryDate" path="insuranceExpiryDate"
								placeholder="MM/DD/YYY" type="text" />
						</div>

						<div class="col-md-6 mb-3">
							<!-- Date input -->
							<label class="control-label" for="date">Permit Expiry
								Date</label>
							<springForm:errors path="permitExpiryDate" class="errors" />
							<springForm:input class="form-control" id="permitExpiryDate"
								path="permitExpiryDate" name="permitExpiryDate"
								placeholder="MM/DD/YYY" type="text" />
						</div>
					</div>
				</section>
				<hr class="my-4">
				<h4 class="card-title">Documents Upload</h4>
				<section class="card-header">
					<div class="form-group required" class="col-sm-4 mb-3">
						<p class="control-label">Registration Certificate :</p>
						<div class="custom-file mb-3">
							<input type="file" class="custom-file-input" id="file1"
								name="file1" /> <label class="custom-file-label" for="file1">Choose
								file </label>
						</div>
					</div>
				</section>
				<br>
				<div class="container">
					<div class="row">
						<div class="col text-center">
							<button type="submit" id="submit" class="btn btn-primary">Submit</button>
						</div>
					</div>
				</div>

			</springForm:form>
		</div>
	</div>


	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/jquery/jquery-3.5.1.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/jquery-validate-1.19.1/dist/jquery.validate.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/jquery-validate-1.19.1/dist/additional-methods.min.js"></script>
	<script type="text/javascript"
		src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript">
		$(function() {
			// Initialize form validation on the registration form.
			// It has the name attribute "registration"
			$("form[id='addVehicle']").validate({
				rules : {
					vehicleNumber : "required",
					vehicleTypes : "required",
					permitExpiryDate : "required",
					insuranceExpiryDate : "required",
				},
				// Specify validation error messages
				messages : {
					vehicleNumber : "Please enter your Vehicle number",
					vehicleTypes : "Please enter your Vehicle type",
					permitExpiryDate : "Please select a date",
					insuranceExpiryDate : "Please select a date"
				},
				submitHandler : function(form) {
					form.submit();
				}
			});

		});

		$.validator.addMethod('filesize', function(value, element, param) {
			return this.optional(element) || (element.files[0].size <= param)
		}, 'File size must be less than {0}');

		$.validator.addMethod("lettersonly", function(value, element) {
			console.log(value);
			return this.optional(element) || /^[A-Z,a-z," ""]+$/i.test(value);
		}, "Invalid character(s). Only Letters are allowed");

		$(function() {
			//			$("form[id='createTap']").validate();
			$("[name^=file]")
					.each(
							function() {
								$(this)
										.rules(
												"add",
												{
													required : true,
													extension : "jpg|jpeg|png",
													filesize : 1000000,
													messages : {
														required : "Please upload required files",
														extension : "Please upload files in jpg, jpeg or png format"
													}
												});
							});
		});

		// Add the following code if you want the name of the file appear on select
		$(".custom-file-input").on(
				"change",
				function() {
					var fileName = $(this).val().split("\\").pop();
					$(this).siblings(".custom-file-label").addClass("selected")
							.html(fileName);
				});
		$(document).ready(
				function() {
					$(function() {
						$("#permitExpiryDate").datepicker().on('changeDate',
								function(ev) {
									$('#permitExpiryDate').datepicker('hide');
								});
						;
					});
				});

		$(document).ready(
				function() {
					$(function() {
						$("#insuranceExpiryDate").datepicker().on(
								'changeDate',
								function(ev) {
									$('#insuranceExpiryDate')
											.datepicker('hide');
								});
						;
					});
				});
	</script>


</body>
</html>