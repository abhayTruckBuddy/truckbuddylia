<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="springForm"
	uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/bootstrap 4.5.0/css/bootstrap.min.css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/ajax/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/ajax/popper.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/bootstrap 4.5.0/js/bootstrap.min.js"></script>
<script>
	$(document).ready(function() {
		$('div:empty').remove();
	});
</script>
<title>Driver Detail</title>

<style type="text/css">
.errors {
	color: red;
}

.form-group.required .control-label:after {
	content: "*";
	color: red;
}

form .error {
	color: #ff0000;
}
</style>
</head>
<body>
	<div id="main-div">

		<div class="jumbotron text-center">
			<h2>Driver Details</h2>
		</div>

		<div id="container">
			<section class="card-header">
				<div style="" class="text-right">
					<button type="button" class="btn btn-secondary"
						onclick="window.location = '${pageContext.request.contextPath}/truckbuddy/lia/mvc/tap/showDriverList'"
						class="btn btn-link">Driver List</button>
				</div>

				<springForm:form action="getDriverList" modelAttribute="driver">

					<springForm:hidden path="mobile" />

					<div class="form-row form-group">
						<div class="col-md-6 mb-3">
							<label for="firstName" class="control-label">First Name:</label>
							<springForm:input type="text" id="firstName" class="form-control"
								path="firstName" name="firstName" />
						</div>
						<div class="col-md-6 mb-3">
							<label for="lastName" class="control-label">Last Name:</label>
							<springForm:input type="text" id="lastName" class="form-control"
								path="lastName" name="lastName" />
						</div>
					</div>

					<div class="form-row form-group">
						<div class="col-md-6 mb-3">
							<label for="email" class="control-label">Email:</label>
							<springForm:input type="text" id="email" class="form-control"
								path="email" name="email" />
						</div>
						<div class="col-md-6 mb-3">
							<label for="aadharCardNumber" class="control-label">Aadhaar
								Card Number:</label>
							<springForm:input type="text" id="aadharCardNumber"
								class="form-control" path="aadharCardNumber"
								name="aadharCardNumber" />
						</div>
					</div>

					<div class="form-row form-group">
						<div class="col-md-6 mb-3">
							<label for="assignedVehicleNumber" class="control-label">Assigned
								Vehicle :</label>
							<springForm:input type="text" id="assignedVehicleNumber"
								class="form-control" path="assignedVehicleNumber.vehicleNumber"
								name="assignedVehicleNumber" />
						</div>
						<div class="col-md-6 mb-3">
							<label for="drivingLicenceNumber" class="control-label">Driving
								License Number</label>
							<springForm:input type="text" id="drivingLicenceNumber"
								class="form-control" path="drivingLicenceNumber"
								name="drivingLicenceNumber" />
						</div>
					</div>
					<hr class="my-4">
					<div class="form-row form-group">
						<div
							class="col-md-6 mb-3 text-center border border-top-0 border-left-0">
							<h5>Driving Licence</h5>
							<img class="rounded img-thumbnail img-fluid"
								src="${pageContext.request.contextPath}/myImage/showDriverDrivingLicence?id=${driver.mobile}&&imageType=drivingLicence" />
						</div>
						<div
							class="col-md-6 mb-3 text-center border border-top-0 border-right-0">
							<h5>Aadhaar Card</h5>
							<img class="rounded img-thumbnail img-fluid"
								src="${pageContext.request.contextPath}/myImage/showDriverAadharCard?id=${driver.mobile}&&imageType=aadharCard" />
						</div>
					</div>

				</springForm:form>
			</section>
		</div>
	</div>
</body>
</html>