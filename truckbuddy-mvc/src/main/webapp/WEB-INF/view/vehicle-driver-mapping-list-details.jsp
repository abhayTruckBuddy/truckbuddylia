<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Driver Vehicle Mapping List</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/DataTables/DataTables-1.10.21/css/dataTables.bootstrap4.min.css">

<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>

<script
	src="${pageContext.request.contextPath}/DataTables/DataTables-1.10.21/js/dataTables.bootstrap4.min.js"></script>


<script type="text/javascript"
	src="${pageContext.request.contextPath}/DataTables/DataTables-1.10.21/js/jquery.dataTables.min.js"></script>


<script>
	$(document).ready(function() {
		$('div:empty').remove();
	});
</script>


<style type="text/css">
.errors {
	color: red;
}

.form-group.required .control-label:after {
	content: "*";
	color: red;
}

form .error {
	color: #ff0000;
}
</style>
</head>
<body>
	<div id="main-div">
		<div class="jumbotron text-center">
			<h2>Driver Vehicle Mapping List</h2>
		</div>

		<div id="tapListDiv">


			<section class="card-header">

				<div style="" class="text-right">
					<button type="button" class="btn btn-secondary"
						onclick="window.location = '${pageContext.request.contextPath}/'"
						class="btn btn-link">Home Page</button>
				</div>
				<table id="tap-table" class="table table-striped table-bordered"
					style="width: 100%">


					<thead>
						<tr>
							<th>Driver Name</th>
							<th>Mobile</th>
							<th>Assigned Vehicle</th>
						</tr>
					</thead>

					<tbody>
						<c:forEach var="driver" items="${driverList}">
							<c:if
								test="${not empty driver.assignedVehicleNumber.vehicleNumber}">
								<tr>
									<td>${driver.firstName}&nbsp;${driver.lastName}</td>
									<td>${driver.mobile}</td>
									<td>${driver.assignedVehicleNumber.vehicleNumber}</td>
								</tr>
							</c:if>
						</c:forEach>
					</tbody>

				</table>

				<div style="" class="text-right">
					<button type="button" class="btn btn-secondary"
						onclick="window.location = '${pageContext.request.contextPath}/'"
						class="btn btn-link">Home Page</button>
				</div>

			</section>
		</div>
	</div>

	<!-- ----------------------------Script Tag---------------------------- -->
	<script>
		$(document).ready(function() {
			$('#tap-table').DataTable();
		});
	</script>

</body>
</html>