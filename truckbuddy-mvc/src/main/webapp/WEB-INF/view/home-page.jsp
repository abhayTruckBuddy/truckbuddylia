<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>TruckBuddy</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/bootstrap 4.5.0/css/bootstrap.min.css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/ajax/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/ajax/popper.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/bootstrap 4.5.0/js/bootstrap.min.js"></script>
</head>
<body>

	<div class="jumbotron text-center">
		<h1>TruckBuddy</h1>
		<p>FTL Loadfinder Logistics Pvt. Ltd.</p>
	</div>

	<div class="container">
		<div class="row text-center">
			<div class="col-sm-6 border border-top-0 border-left-0">
				<button type="button"
					onclick="window.location = 'truckbuddy/lia/mvc/tap/showTapList'"
					class="btn btn-link">View TAP(s)</button>
				<p>Click to view all TAP(s)</p>
			</div>
			<div class="col-sm-6 border border-right-0 border-top-0">
				<button type="button"
					onclick="window.location = 'truckbuddy/lia/mvc/tap/tapRegistationForm'"
					class="btn btn-link">Add TAP</button>
				<p>Click to add new TAP</p>
			</div>
		</div>

		<div class="row text-center">
			<div class="col-sm-6 border border-top-0 border-left-0">
				<button type="button"
					onclick="window.location = '/truckbuddy/lia/mvc/tap/showDriverList'"
					class="btn btn-link">View Driver(s)</button>
				<p>Click to view all Driver(s)</p>
			</div>
			<div class="col-sm-6 border border-right-0 border-top-0">
				<button type="button"
					onclick="window.location = '/truckbuddy/lia/mvc/tap/driverRegistationForm'"
					class="btn btn-link">Add Driver</button>
				<p>Click to add new Driver</p>
			</div>
		</div>

		<div class="row text-center">
			<div class="col-sm-6 border border-top-0 border-left-0">
				<button type="button"
					onclick="window.location = '/truckbuddy/lia/mvc/tap/showVehicleList'"
					class="btn btn-link">View Vehicle(s)</button>
				<p>Click to view all Vehicle(s)</p>
			</div>
			<div class="col-sm-6 border border-right-0 border-top-0">
				<button type="button"
					onclick="window.location = '/truckbuddy/lia/mvc/tap/vehicleRegistationForm'"
					class="btn btn-link">Add Vehicle</button>
				<p>Click to add new Vehicle</p>
			</div>
		</div>

		<div class="row text-center">
			<div class="col-sm border border-right-0 border-left-0">
				<button type="button"
					onclick="window.location = '/truckbuddy/lia/mvc/tap/showVehicleDriverMappingForm'"
					class="btn btn-link">Vehicle-Driver Association</button>
				<p>Click to associate Vehicle to Driver & view all Vehicle
					Driver Association(s)</p>
			</div>
		</div>



	</div>

</body>
</html>