<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="springForm"
	uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/bootstrap 4.5.0/css/bootstrap.min.css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/ajax/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/ajax/popper.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/bootstrap 4.5.0/js/bootstrap.min.js"></script>
<title>TAP Registration Form</title>


<script>
	$(document).ready(function() {
		$('div:empty').remove();
	});
</script>

<style type="text/css">
.errors {
	color: red;
}

.form-group.required .control-label:after {
	content: "*";
	color: red;
}

form .error {
	color: #ff0000;
}
</style>

</head>
<body>

	<div class="jumbotron text-center">
		<h2>TAP Registration Form</h2>
	</div>
	<div class="container">
		<div style="" class="text-right">
			<button type="button" class="btn btn-secondary"
				onclick="window.location = '${pageContext.request.contextPath}/'"
				class="btn btn-link">Home Page</button>
		</div>
		<div id="errorCardBody" name="errorCardBody" class="errors"></div>
		<c:if test="${uploadError}">
			<div class="alert alert-danger col-xs-offset-1 col-xs-10">
				Error while upload File(s). Please try again.</div>
		</c:if>

		<springForm:form name="tap-registraion-form" action="createTap"
			id="createTap" modelAttribute="tap" method="POST"
			enctype="multipart/form-data">

			<h4 class="card-title">Basic Details</h4>
			<section class="card-header">
				<div class="form-row form-group required">
					<div class="col-md-6 mb-3">
						<label for="firstName" class="control-label">First Name :</label>
						<springForm:errors path="firstName" class="errors" />
						<springForm:input path="firstName" type="text"
							class="form-control" id="firstName"
							placeholder="Enter First Name" name="firstName" />
					</div>

					<div class="col-md-6 mb-3">
						<label for="lastName" class="control-label">Last Name :</label>
						<springForm:errors path="lastName" class="errors" />
						<springForm:input path="lastName" type="text" class="form-control"
							id="lastName" placeholder="Enter Last Name" name="lastName" />
					</div>
				</div>


				<div class="form-row form-group required">
					<div class="col-md-6 mb-3">
						<label for="mobile" class="control-label">Mobile Number :</label>
						<springForm:input type="text" class="form-control" id="mobile"
							path="mobile" placeholder="Enter Mobile Number" name="mobile" />
						<springForm:errors path="mobile" class="errors" />
					</div>
					<div class="col-md-6 mb-3">
						<label for="alternateMobile">Alternate Contact Number :</label>
						<springForm:errors path="alternateMobile" class="errors" />
						<springForm:input type="text" class="form-control"
							path="alternateMobile" id="alternateMobile"
							placeholder="Enter Alternate Contact Number"
							name="alternateMobile" />
					</div>
				</div>

				<div class="form-group">
					<label for="email">Email :</label>
					<springForm:errors path="email" class="errors" />
					<springForm:input type="text" class="form-control" id="email"
						path="email" placeholder="Enter Email" name="email" />
				</div>

				<div class="form-group required" class="col-sm-4 mb-3">
					<label for="address" class="control-label">Address :</label>
					<springForm:errors path="address" class="errors" />
					<springForm:textarea type="text" class="form-control"
						path="address" id="address" placeholder="Enter Address"
						name="address" />
				</div>

				<div class="form-row form-group required">
					<div class="col-md-6 mb-3">
						<label for="city" class="control-label">City :</label>
						<springForm:errors path="city" class="errors" />
						<springForm:input type="text" class="form-control" id="city"
							path="city" placeholder="City" name="city" />
					</div>
					<div class="col-md-6 mb-3">
						<label for="state" class="control-label">State :</label>
						<springForm:errors path="state" class="errors" />
						<springForm:input type="text" class="form-control" id="state"
							path="state" placeholder="State" name="state" />
					</div>
				</div>


				<div class="form-row form-group required">
					<div class="col-md-6 mb-3">
						<label for="panCard" class="control-label">PAN Card Number
							:</label>
						<springForm:errors path="panCard" class="errors" />
						<springForm:input type="text" class="form-control" id="panCard"
							path="panCard" placeholder="PAN Card Number" name="panCard" />
					</div>
					<div class="col-md-6 mb-3">
						<label for="aadharCard" class="control-label">Aadhaar Card
							Number :</label>
						<springForm:errors path="aadharCard" class="errors" />
						<springForm:input type="text" class="form-control" id="aadharCard"
							path="aadharCard" placeholder="Aadhaar Card Number"
							name="aadharCard" />
					</div>
				</div>


				<div class="form-row form-group required">
					<div class="col-md-6 mb-3">
						<label for="companyName" class="control-label">Company
							Name : </label>
						<springForm:errors path="companyName" class="errors" />
						<springForm:input type="text" class="form-control"
							path="companyName" id="companyName" name="companyName"
							placeholder="Enter Company Name" />
					</div>
					<div class="col-md-6 mb-3">
						<label for="gstNumber" class="control-label">GST Number :
						</label>
						<springForm:errors path="gstNumber" class="errors" />
						<springForm:input type="text" class="form-control"
							path="gstNumber" id="gstNumber" placeholder="GST Number"
							name="gstNumber" />
					</div>
				</div>


				<div class="form-group required" class="col-sm-4 mb-3">
					<label for="loadTypeId" class="control-label">Load Type :</label>
					<springForm:errors path="loadTypeId" class="errors" />
					<springForm:select path="loadTypeId" class="form-control"
						name="loadTypeId" id="loadTypeId">
						<springForm:option value="" label="--Select Load Type--"></springForm:option>
						<springForm:option value="101" label="Load Type 1"></springForm:option>
						<springForm:option value="102" label="Load Type 2"></springForm:option>
					</springForm:select>
				</div>
			</section>
			<hr class="my-4">

			<h4 class="card-title">Bank Account Details</h4>
			<section class="card-header">
				<div class="form-group required">
					<label for="bankAccountNumber" class="control-label">Account
						Number :</label>
					<springForm:errors path="${bankAccountDetails.bankAccountNumber}"
						class="errors" />
					<springForm:input type="text" class="form-control"
						id="bankAccountNumber"
						path="${bankAccountDetails.bankAccountNumber}"
						placeholder="Enter Account Number" name="bankAccountNumber" />
				</div>

				<div class="form-row form-group required">
					<div class="col-md-6 mb-3">
						<label for="accountHolderName" class="control-label">Account
							Holder Name :</label>
						<springForm:errors path="${bankAccountDetails.accountHolderName}"
							class="errors" />
						<springForm:input type="text" class="form-control"
							id="accountHolderName"
							path="${bankAccountDetails.accountHolderName}"
							placeholder="Enter Account Holder Name" name="accountHolderName" />
					</div>

					<div class="col-md-6 mb-3">
						<label for="bankName" class="control-label">Bank Name :</label>
						<springForm:errors path="${bankAccountDetails.bankName}"
							class="errors" />
						<springForm:input type="text" class="form-control" id="bankName"
							path="${bankAccountDetails.bankName}"
							placeholder="Enter Bank Name" name="bankName" />
					</div>
				</div>

				<div class="form-row form-group required">
					<div class="col-md-6 mb-3">
						<label for="ifscCode" class="control-label">IFSC Code :</label>
						<springForm:errors path="${bankAccountDetails.ifscCode}"
							class="errors" />
						<springForm:input path="${bankAccountDetails.ifscCode}"
							type="text" class="form-control" id="ifscCode"
							placeholder="Enter IFSC Code" name="ifscCode" />

					</div>

					<div class="col-md-6 mb-3 ">
						<label for="accountType" class="control-label">Account
							Type :</label>
						<springForm:errors path="${bankAccountDetails.accountType}"
							class="errors" />
						<springForm:select path="${bankAccountDetails.accountType}"
							class="form-control" name="accountType" id="accountType">
							<springForm:option value="" label="--Select Account Type--"></springForm:option>
							<springForm:option value="SAVING" label="Saving Type"></springForm:option>
							<springForm:option value="CURRENT" label="Current Type"></springForm:option>
							<springForm:option value="OTHERS" label="Other"></springForm:option>
						</springForm:select>

					</div>
				</div>

			</section>
			<hr class="my-4">
			<h4 class="card-title">Documents Upload</h4>
			<section class="card-header">
				<div class="form-group required" class="col-sm-4 mb-3">
					<p class="control-label">Upload Profile Picture :</p>
					<div class="custom-file mb-3">
						<input type="file" class="custom-file-input" id="file1"
							name="file1" /> <label class="custom-file-label" for="file">Choose
							file</label>
					</div>
				</div>


				<div class="form-group required" class="col-sm-4 mb-3">
					<p class="control-label">Agreement Copy :</p>
					<div class="custom-file mb-3">
						<input type="file" class="custom-file-input" id="file2"
							name="file2" /> <label class="custom-file-label" for="file">Choose
							file</label>
					</div>
				</div>

				<div class="form-group required" class="col-sm-4 mb-3">
					<p class="control-label">GST Certificate :</p>
					<div class="custom-file mb-3">
						<input type="file" class="custom-file-input" id="file3"
							name="file3" /> <label class="custom-file-label" for="file">Choose
							file</label>
					</div>
				</div>

				<div class="form-group required" class="col-sm-4 mb-3">
					<p class="control-label">Cancelled Cheque :</p>
					<div class="custom-file mb-3">
						<input type="file" class="custom-file-input" id="file4"
							name="file4" /> <label class="custom-file-label" for="file">Choose
							file</label>
					</div>
				</div>
			</section>
			<br>
			<div class="container">
				<div class="row">
					<div class="col text-center">
						<button type="submit" id="submit" class="btn btn-primary">Submit</button>
					</div>
				</div>
			</div>
		</springForm:form>
	</div>

	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/jquery/jquery-3.5.1.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/jquery-validate-1.19.1/dist/jquery.validate.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/jquery-validate-1.19.1/dist/additional-methods.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/tap-form-validation.js"></script>
	<script type="text/javascript">
		$(function() {
			// Initialize form validation on the registration form.
			// It has the name attribute "registration"
			$("form[id='createTap']")
					.validate(
							{
								rules : {
									firstName : {
										required : true,
										lettersonly : true
									},
									lastName : {
										required : true,
										lettersonly : true
									},
									mobile : {
										required : true,
										number : true,
										minlength : 10,
										maxlength : 10,
										digits : true,
									},
									alternateMobile : {
										number : true,
										minlength : 10,
										maxlength : 10,
										digits : true,
									},
									email : {
										email : true
									},
									address : {
										required : true
										},
									city : {
										required : true,
										lettersonly : true
									},
									state : {
										required : true,
										lettersonly : true
									},
									gstNumber : "required",
									companyName : "required",
									panCard : "required",
									aadharCard : {
										required : true,
										number : true,
										minlength : 16,
										maxlength : 16,
										digits : true,
									},
									loadTypeId : "required",
									// file: "required",
									bankAccountNumber : {
										required : true,
										number : true,
										minlength : 10,
										maxlength : 20,
										digits : true,
									},
									accountHolderName : {
										required : true,
										lettersonly : true
									},
									bankName : {
										required : true,
										lettersonly : true
									},
									ifscCode : "required",
									accountType : "required"
								},
								// Specify validation error messages
								messages : {
									firstName : "Please enter your first name",
									lastName : "Please enter your last name",
									mobile : {
										required : "Please enter your mobile number",
										minlength : "Invalid mobile number",
										maxlength : "Invalid mobile number",
										number : "Invalid mobile number",
										digits : "Invalid number or character",
									},
									alternateMobile : {
										minlength : "Invalid mobile number",
										maxlength : "Invalid mobile number",
										number : "Invalid mobile number",
										digits : "Invalid number or character",
									},
									email : "Please enter a valid email address",
									address : "Please enter your address",
									city : "Please enter your city",
									state : "Please enter your state",
									gstNumber : "Please enter your GST number",
									companyName : "Please enter your company name",
									panCard : "Please enter your PAN Number",
									aadharCard : {
										required : "Please enter your Aadhar card Number",
										number : "Invalid Aadhar card number",
										minlength : "Invalid Aadhar card number",
										maxlength : "Invalid Aadhar card number",
										digits : "Invalid number or character",
									},
									loadTypeId : "Please select load type",
									bankAccountNumber : {
										required : "Please enter your Account Number",
										number : "Invalid Account number",
										minlength : "Invalid Account number",
										maxlength : "Invalid Account number",
										digits : "Invalid number or character",
									},
									accountHolderName : "Please enter your Account Holder Name",
									bankName : "Please enter your Bank name",
									ifscCode : "Please enter your IFSC code",
									accountType : "Please enter your Account type"
								},
								submitHandler : function(form) {
									form.submit();
								}
							});

		});

		$.validator.addMethod('filesize', function(value, element, param) {
			return this.optional(element) || (element.files[0].size <= param)
		}, 'File size must be less than {0}');

		$.validator.addMethod("lettersonly", function(value, element) {
			console.log(value);
			return this.optional(element) || /^[A-Z,a-z," ""]+$/i.test(value);
		}, "Invalid character(s). Only Letters are allowed");

		$(function() {
			//			$("form[id='createTap']").validate();
			$("[name^=file]")
					.each(
							function() {
								$(this)
										.rules(
												"add",
												{
													required : true,
													extension : "jpg|jpeg|png",
													filesize : 1000000,
													messages : {
														required : "Please upload required files",
														extension : "Please upload files in jpg, jpeg or png format"
													}
												});
							});
		});
		// Add the following code if you want the name of the file appear on select
		$(".custom-file-input").on(
				"change",
				function() {
					var fileName = $(this).val().split("\\").pop();
					$(this).siblings(".custom-file-label").addClass("selected")
							.html(fileName);
				});
	</script>


</body>
</html>