<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="springForm"
	uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/bootstrap 4.5.0/css/bootstrap.min.css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/ajax/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/ajax/popper.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/bootstrap 4.5.0/js/bootstrap.min.js"></script>
<script>
	$(document).ready(function() {
		$('div:empty').remove();
	});
</script>

<style type="text/css">
.errors {
	color: red;
}

.form-group.required .control-label:after {
	content: "*";
	color: red;
}

form .error {
	color: #ff0000;
}
</style>
<title>TAP Details</title>
</head>
<body>



	<div id="main-div">

		<div class="jumbotron text-center">
			<h2>TAP Details</h2>
		</div>

		<div id="container">
			<section class="card-header">

				<div style="" class="text-right">
					<button type="button" class="btn btn-secondary"
						onclick="window.location = '${pageContext.request.contextPath}/truckbuddy/lia/mvc/tap/showTapList'"
						class="btn btn-link">TAP(s) List</button>
				</div>
				<springForm:form action="getTaps" modelAttribute="tap">

					<springForm:hidden path="mobile" />

					<div class="form-row form-group">
						<div class="col-md-6 mb-3">
							<label for="firstName" class="control-label">First Name:</label>
							<springForm:input type="text" id="firstName" class="form-control"
								path="firstName" name="firstName" />
						</div>
						<div class="col-md-6 mb-3">
							<label for="lastName" class="control-label">Last Name:</label>
							<springForm:input type="text" id="lastName" class="form-control"
								path="lastName" name="lastName" />
						</div>
					</div>


					<div class="form-row form-group">
						<div class="col-md-6 mb-3">
							<label for="mobile" class="control-label">Mobile</label>
							<springForm:input type="text" id="mobile" class="form-control"
								path="mobile" name="mobile" />
						</div>
						<div class="col-md-6 mb-3">
							<label for="alternateMobile" class="control-label">Alternate
								Mobile</label>
							<springForm:input type="text" id="alternateMobile"
								class="form-control" path="alternateMobile"
								name="alternateMobile" />
						</div>
					</div>

					<div class="form-group">
						<label for="email">Email :</label>
						<springForm:input path="email" class="form-control" />
					</div>

					<div class="form-row form-group">
						<div class="col-md-6 mb-3">
							<label for="state" class="control-label">State</label>
							<springForm:input type="text" id="state" class="form-control"
								path="state" name="state" />
						</div>
						<div class="col-md-6 mb-3">
							<label for="city" class="control-label">City</label>
							<springForm:input type="text" id="city" class="form-control"
								path="city" name="city" />
						</div>
					</div>


					<div class="form-row form-group">
						<div class="col-md-6 mb-3">
							<label for="panCard" class="control-label">PAN Card</label>
							<springForm:input type="text" id="panCard" class="form-control"
								path="panCard" name="panCard" />
						</div>
						<div class="col-md-6 mb-3">
							<label for="aadharCard" class="control-label">Aadhaar
								Card</label>
							<springForm:input type="text" id="aadharCard"
								class="form-control" path="aadharCard" name="aadharCard" />
						</div>
					</div>


					<div class="form-row form-group">
						<div
							class="col-md-3 mb-3 text-center border border-top-0 border-left-0">
							<h5>Profile Pic</h5>
							<img class="rounded img-thumbnail img-fluid"
								src="${pageContext.request.contextPath}/myImage/showTapProfilePic?id=${tap.mobile}&&imageType=profilePic" />

						</div>
						<div
							class="col-md-3 mb-3 text-center border border-top-0 border-left-0 border-rightt-0">
							<h5>Agreement Copy</h5>
							<img class="rounded img-thumbnail img-fluid"
								src="${pageContext.request.contextPath}/myImage/showTapAgreementCopy?id=${tap.mobile}&&imageType=agreementCopy" />

						</div>
						<div
							class="col-md-3 mb-3 text-center border border-top-0 border-left-0 border-rightt-0">
							<h5>GST Certificate</h5>
							<img class="rounded img-thumbnail img-fluid"
								src="${pageContext.request.contextPath}/myImage/showTapGstCertificate?id=${tap.mobile}&&imageType=gstCertificate" />

						</div>
						<div
							class="col-md-3 mb-3 text-center border border-top-0 border-right-0">
							<h5>Bank Cancelled Cheque</h5>
							<img class="rounded img-thumbnail img-fluid"
								src="${pageContext.request.contextPath}/myImage/showTapCancelledCheque?id=${tap.mobile}&&imageType=profilePic" />

						</div>
					</div>
				</springForm:form>
			</section>
		</div>
	</div>

</body>
</html>