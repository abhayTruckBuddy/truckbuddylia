package com.example.lia.truckbuddy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.example.lia.truckbuddy.entity.Vehicle;

public interface VehicleRepository extends JpaRepository<Vehicle, String> {
	
	 List<Vehicle> findByTapMobile(@Param("tapId") String tapId);

}
