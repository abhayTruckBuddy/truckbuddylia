package com.example.lia.truckbuddy.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.lia.truckbuddy.entity.Vehicle;
import com.example.lia.truckbuddy.repository.VehicleRepository;

@Service
public class VehicleServiceImpl implements VehicleService {

	@Autowired
	private VehicleRepository vehicleRepository;

	@Override
	public void saveVehicle(Vehicle vehicle) {
		vehicleRepository.save(vehicle);

	}

	@Override
	public Vehicle getVehicle(String vehicleNumber) {
		Optional<Vehicle> result = vehicleRepository.findById(vehicleNumber);

		if (result.isPresent()) {
			return result.get();
		} else {
			throw new RuntimeException("Vehicle not found with Vehicle Number : " + vehicleNumber);
		}
	}

	@Override
	public List<Vehicle> getVehicle() {
		return vehicleRepository.findAll();
	}

	@Override
	public List<Vehicle> getVehiclesByTapId(String tapId) {
		return vehicleRepository.findByTapMobile(tapId);
	}
}
