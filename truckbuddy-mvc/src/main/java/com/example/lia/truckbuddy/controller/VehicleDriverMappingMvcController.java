package com.example.lia.truckbuddy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.lia.truckbuddy.entity.Driver;
import com.example.lia.truckbuddy.entity.Vehicle;
import com.example.lia.truckbuddy.service.DriverService;
import com.example.lia.truckbuddy.service.VehicleService;

@Controller
@RequestMapping("/truckbuddy/lia/mvc/tap")
public class VehicleDriverMappingMvcController {

	private final VehicleService vehicleService;
	private final DriverService driverService;

	@Autowired
	public VehicleDriverMappingMvcController(VehicleService vehicleService, DriverService driverService) {
		this.driverService = driverService;
		this.vehicleService = vehicleService;
	}

	@GetMapping("/showVehicleDriverMappingList")
	private String showVehicleDriverMappingList(Model model) {
		model.addAttribute("driverList", driverService.getDriversByTapId("9900000001"));

		return "vehicle-driver-mapping-list-details";
	}

	@GetMapping("/showVehicleDriverMappingForm")
	private String showVehicleDriverMappingForm(Model model) {
		model.addAttribute("driverList", driverService.getDriversByTapId("9900000001"));
		model.addAttribute("vehicleList", vehicleService.getVehiclesByTapId("9900000001"));
		return "vehicle-driver-mapping-form";
	}

	@PostMapping("/addVehicleDriverMapping")
	private String addVehicleDriverMapping(@RequestParam("driverId") String driverId,
			@RequestParam("vehicleNumber") String vehicleNumber, Model model) {
		if (driverId.isEmpty() || vehicleNumber.isEmpty()) {
			model.addAttribute("formError", true);
			return "vehicle-driver-mapping-form";
		}
		Vehicle vehicle = vehicleService.getVehicle(vehicleNumber);
		Driver driver = driverService.getDriver(driverId);
		driver.setAssignedVehicleNumber(vehicle);
		driverService.saveDriver(driver);
		return "redirect:/truckbuddy/lia/mvc/tap/showVehicleDriverMappingForm";
	}

	@GetMapping("/disassociateVehicleDriver")
	public String disassociateVehicleDriver(@RequestParam("mobile") String mobile) {
		Driver driver = driverService.getDriver(mobile);
		Vehicle vehicle = vehicleService.getVehicle(driver.getAssignedVehicleNumber().getVehicleNumber());
		vehicle.setDriver(null);
		driver.setAssignedVehicleNumber(null);
		vehicleService.saveVehicle(vehicle);
		driverService.saveDriver(driver);
		return "redirect:/truckbuddy/lia/mvc/tap/showVehicleDriverMappingForm";
	}

}
