package com.example.lia.truckbuddy.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tap_booking_request")
public class TapBookingRequest {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name = "tap_id", nullable = false)
	private String tap;
	@Column(name = "booking_request_id", nullable = false)
	private String bookingRequestId;
	@Column(name = "booking_response_id")
	private String bookingResponseId;

	public TapBookingRequest() {
		// TODO Auto-generated constructor stub
	}

	public TapBookingRequest(String tap, String bookingRequestId, String bookingResponseId) {
		this.tap = tap;
		this.bookingRequestId = bookingRequestId;
		this.bookingResponseId = bookingResponseId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTap() {
		return tap;
	}

	public void setTap(String tap) {
		this.tap = tap;
	}

	public String getBookingRequestId() {
		return bookingRequestId;
	}

	public void setBookingRequestId(String bookingRequestId) {
		this.bookingRequestId = bookingRequestId;
	}

	public String getBookingResponseId() {
		return bookingResponseId;
	}

	public void setBookingResponseId(String bookingResponseId) {
		this.bookingResponseId = bookingResponseId;
	}

	@Override
	public String toString() {
		return "TapBookingRequest [id=" + id + ", tap=" + tap + ", bookingRequestId=" + bookingRequestId
				+ ", bookingResponseId=" + bookingResponseId + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TapBookingRequest other = (TapBookingRequest) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
