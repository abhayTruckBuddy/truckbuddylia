package com.example.lia.truckbuddy.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "booking_request")
public class BookingRequest {

	@Id
	@Column(name = "booking_request_id")
	private String id;
	@Column(name = "source", nullable = false)
	private String source;
	@Column(name = "destination", nullable = false)
	private String destination;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "booking_request_date", nullable = false)
	private Date bookingRequestDate;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "booking_date", nullable = false)
	private Date bookingDate;
	@Column(name = "fare", nullable = false, precision = 10, scale = 2)
	private BigDecimal fare;
	@Column(name = "status", nullable = false)
	private String status;
	@OneToOne
	@JoinColumn(name = "vehicle_type")
	private VehicleType vehicleType;
	@OneToOne
	@JoinColumn(name = "load_type")
	private LoadType loadType;

	public BookingRequest() {
		// TODO Auto-generated constructor stub
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Date getBookingRequestDate() {
		return bookingRequestDate;
	}

	public void setBookingRequestDate(Date bookingRequestDate) {
		this.bookingRequestDate = bookingRequestDate;
	}

	public Date getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}

	public BigDecimal getFare() {
		return fare;
	}

	public void setFare(BigDecimal fare) {
		this.fare = fare;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public VehicleType getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(VehicleType vehicleType) {
		this.vehicleType = vehicleType;
	}

	public LoadType getLoadType() {
		return loadType;
	}

	public void setLoadType(LoadType loadType) {
		this.loadType = loadType;
	}

	@Override
	public String toString() {
		return "BookingRequest [id=" + id + ", source=" + source + ", destination=" + destination
				+ ", bookingRequestDate=" + bookingRequestDate + ", bookingDate=" + bookingDate + ", fare=" + fare
				+ ", status=" + status + ", vehicleType=" + vehicleType + ", loadType=" + loadType + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BookingRequest other = (BookingRequest) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
