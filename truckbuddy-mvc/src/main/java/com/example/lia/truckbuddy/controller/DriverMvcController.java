package com.example.lia.truckbuddy.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.example.lia.truckbuddy.common.FileUploadHelper;
import com.example.lia.truckbuddy.entity.Driver;
import com.example.lia.truckbuddy.entity.Tap;
import com.example.lia.truckbuddy.entity.Vehicle;
import com.example.lia.truckbuddy.service.DriverService;
import com.example.lia.truckbuddy.service.VehicleService;

@Controller
@RequestMapping("/truckbuddy/lia/mvc/tap")
public class DriverMvcController {

	@Autowired
	private DriverService driverService;
	@Autowired
	private VehicleService vehicleService;
	@Autowired
	private FileUploadHelper fileUploadHelper;

	@RequestMapping("/driverRegistationForm")
	public String showDriverRegistrationForm(Model model) {
		Driver driver = new Driver();
		List<Vehicle> vehicleList = vehicleService.getVehiclesByTapId("9900000001");
		model.addAttribute("driver", driver);
		model.addAttribute("vehicleList", vehicleList);
		return "driver-registration-form";
	}

	@PostMapping("/addDriver")
	public String addDriver(@Valid @ModelAttribute("driver") Driver driver, BindingResult bindingResult,
			@RequestParam("vehicleNumber") String vehicleNumber, @RequestParam("file1") MultipartFile drivingLicence,
			@RequestParam("file2") MultipartFile aadharCard, Model model) {

		if (bindingResult.hasErrors()) {
			return "driver-registration-form";
		}

		try {
			if (!vehicleNumber.isEmpty()) {
				Vehicle vehicle = vehicleService.getVehicle(vehicleNumber);
				driver.setAssignedVehicleNumber(vehicle);
			}
			driver.setDrivingLicence(fileUploadHelper.uploadFile(drivingLicence));
			driver.setAadharCard(fileUploadHelper.uploadFile(aadharCard));
			Tap tap = new Tap();
			tap.setMobile("9900000001");
			driver.setTap(tap);
			driverService.saveDriver(driver);
			if (driver.getDrivingLicence() == null || driver.getAadharCard() == null) {
				model.addAttribute("uploadError", true);
				return "driver-registration-form";
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			model.addAttribute("uploadError", true);
			return "driver-registration-form";
		}
		return "redirect:/truckbuddy/lia/mvc/tap/showDriverList";

	}

	@GetMapping("/showDriverList")
	public String getDriverList(Model model) {
		List<Driver> driverList = driverService.getDrivers();
		model.addAttribute("driverList", driverList);
		return "driver-list";
	}

	@GetMapping("/showDriverDetails")
	public String showTapDetails(@RequestParam("mobile") String mobile, Model model) {
		Driver driver = driverService.getDriver(mobile);
		model.addAttribute("driver", driver);
		return "driver-details";
	}

}
