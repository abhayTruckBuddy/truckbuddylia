package com.example.lia.truckbuddy.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URLConnection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.lia.truckbuddy.entity.Driver;
import com.example.lia.truckbuddy.entity.Tap;
import com.example.lia.truckbuddy.entity.Vehicle;
import com.example.lia.truckbuddy.service.DriverService;
import com.example.lia.truckbuddy.service.TapService;
import com.example.lia.truckbuddy.service.VehicleService;

@Controller
@RequestMapping("/myImage")
public class ImageController {

	@Autowired
	private TapService tapService;

	@Autowired
	private DriverService driverService;

	@Autowired
	private VehicleService vehicleService;

	@GetMapping(value = "/showTapProfilePic")
	public void showTapProfilePic(@RequestParam("id") String itemId, HttpServletResponse response,
			HttpServletRequest request) throws ServletException, IOException {

		Tap tap = tapService.getTap(itemId);
		String contentType = URLConnection.guessContentTypeFromStream(new ByteArrayInputStream(tap.getProfilePic()));

		response.setContentType(contentType);
		response.getOutputStream().write(tap.getProfilePic());
		response.getOutputStream().close();
	}

	@GetMapping(value = "/showTapAgreementCopy")
	public void showTapAgreementCopy(@RequestParam("id") String itemId, HttpServletResponse response,
			HttpServletRequest request) throws ServletException, IOException {

		Tap tap = tapService.getTap(itemId);
		String contentType = URLConnection.guessContentTypeFromStream(new ByteArrayInputStream(tap.getAgreementCopy()));

		response.setContentType(contentType);
		response.getOutputStream().write(tap.getAgreementCopy());
		response.getOutputStream().close();
	}

	@GetMapping(value = "/showTapGstCertificate")
	public void showTapGstCertificate(@RequestParam("id") String itemId, HttpServletResponse response,
			HttpServletRequest request) throws ServletException, IOException {

		Tap tap = tapService.getTap(itemId);
		String contentType = URLConnection
				.guessContentTypeFromStream(new ByteArrayInputStream(tap.getGstCertificate()));

		response.setContentType(contentType);
		response.getOutputStream().write(tap.getGstCertificate());
		response.getOutputStream().close();
	}

	@GetMapping(value = "/showTapCancelledCheque")
	public void showTapBankCancelledCheque(@RequestParam("id") String itemId, HttpServletResponse response,
			HttpServletRequest request) throws ServletException, IOException {

		Tap tap = tapService.getTap(itemId);
		String contentType = URLConnection
				.guessContentTypeFromStream(new ByteArrayInputStream(tap.getBankAccountDetails().getCancelledCheque()));

		response.setContentType(contentType);
		response.getOutputStream().write(tap.getBankAccountDetails().getCancelledCheque());
		response.getOutputStream().close();
	}

	@GetMapping(value = "/showDriverDrivingLicence")
	public void showDriverDrivingLicence(@RequestParam("id") String itemId, HttpServletResponse response,
			HttpServletRequest request) throws ServletException, IOException {

		Driver driver = driverService.getDriver(itemId);
		String contentType = URLConnection
				.guessContentTypeFromStream(new ByteArrayInputStream(driver.getDrivingLicence()));
		response.setContentType(contentType);
		response.getOutputStream().write(driver.getDrivingLicence());
		response.getOutputStream().close();
	}

	@GetMapping(value = "/showDriverAadharCard")
	public void showDriverAadharCard(@RequestParam("id") String itemId, HttpServletResponse response,
			HttpServletRequest request) throws ServletException, IOException {

		Driver driver = driverService.getDriver(itemId);
		String contentType = URLConnection.guessContentTypeFromStream(new ByteArrayInputStream(driver.getAadharCard()));

		response.setContentType(contentType);
		response.getOutputStream().write(driver.getAadharCard());
		response.getOutputStream().close();
	}

	@GetMapping(value = "/showVehicleRegistrationCertificate")
	public void showVehicleRegistrationCertificate(@RequestParam("id") String itemId, HttpServletResponse response,
			HttpServletRequest request) throws ServletException, IOException {

		Vehicle vehicle = vehicleService.getVehicle(itemId);
		String contentType = URLConnection.guessContentTypeFromStream(new ByteArrayInputStream(vehicle.getRegistrationCertificate()));

		response.setContentType(contentType);
		response.getOutputStream().write(vehicle.getRegistrationCertificate());
		response.getOutputStream().close();
	}

}
