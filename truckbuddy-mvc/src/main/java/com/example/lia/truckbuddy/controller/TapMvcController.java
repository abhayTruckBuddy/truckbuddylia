package com.example.lia.truckbuddy.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.example.lia.truckbuddy.common.FileUploadHelper;
import com.example.lia.truckbuddy.entity.BankAccountDetails;
import com.example.lia.truckbuddy.entity.Tap;
import com.example.lia.truckbuddy.service.TapService;

@Controller
@RequestMapping("/truckbuddy/lia/mvc/tap")
public class TapMvcController {

	@Autowired
	private TapService tapService;
	@Autowired
	private FileUploadHelper fileUploadHelper;

	@RequestMapping("/tapRegistationForm")
	public String getTapRegistationForm(Model model) {
		BankAccountDetails bankAccountDetails = new BankAccountDetails();
		Tap tap = new Tap();
		tap.setBankAccountDetails(bankAccountDetails);
		model.addAttribute("tap", tap);
		model.addAttribute("bankAccountDetails", bankAccountDetails);
		System.out.println("Redirecting to tap-registration-form...");
		return "tap-registration-form";
	}

	@PostMapping(value = "/createTap")
	public String createTap(@Valid @ModelAttribute("tap") Tap tap, BindingResult bindingResult,
			@Valid @ModelAttribute("bankAccountDetails") BankAccountDetails bankAccountDetails,
			BindingResult bankDetailsBindingResult, @RequestParam("file1") MultipartFile profilePic,
			@RequestParam("file2") MultipartFile agreement, @RequestParam("file3") MultipartFile gstCertificate,
			@RequestParam("file4") MultipartFile cancelledCheque, Model model) {

		if (bindingResult.hasErrors() || bankDetailsBindingResult.hasErrors()) {
			return "tap-registration-form";
		}
		try {
			tap.setProfilePic(fileUploadHelper.uploadFile(profilePic));
			tap.setAgreementCopy(fileUploadHelper.uploadFile(agreement));
			tap.setGstCertificate(fileUploadHelper.uploadFile(gstCertificate));
			bankAccountDetails.setCancelledCheque(fileUploadHelper.uploadFile(cancelledCheque));
			tap.setBankAccountDetails(bankAccountDetails);
			tapService.saveTap(tap);

			if (tap.getAgreementCopy() == null || tap.getProfilePic() == null || tap.getGstCertificate() == null
					|| tap.getBankAccountDetails().getCancelledCheque() == null) {
				model.addAttribute("uploadError", true);
				return "tap-registration-form";
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			model.addAttribute("uploadError", true);
			return "tap-registration-form";
		}
		return "redirect:/truckbuddy/lia/mvc/tap/showTapList";
	}

	@GetMapping("/showTapList")
	public String getAllTaps(Model model) {
		List<Tap> tapList = tapService.getTaps();
		model.addAttribute("tapList", tapList);
		return "tap-list";
	}

	@GetMapping("/showTapDetails")
	public String showTapDetails(@RequestParam("mobileNumber") String mobileNumber, Model model) {
		Tap tap = tapService.getTap(mobileNumber);
		model.addAttribute("tap", tap);
		return "tap-details";
	}

}
