package com.example.lia.truckbuddy.service;

import java.util.List;

import com.example.lia.truckbuddy.entity.Vehicle;

public interface VehicleService {

	public void saveVehicle(Vehicle vehicle);
	
	public Vehicle getVehicle(String vehicleNumber);

	public List<Vehicle> getVehicle();
	
	public List<Vehicle> getVehiclesByTapId(String tapId);

}
