package com.example.lia.truckbuddy.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.springframework.data.domain.Persistable;

@Entity
@Table(name = "driver")
public class Driver implements Persistable<String> {

	@Id
	@Column(name = "mobile")
	@Size(min = 10, max = 13, message = "must be a valid Mobile Number")
	private String mobile;
	@Column(name = "first_name")
	@NotEmpty(message = "is required")
	private String firstName;
	@Column(name = "last_name")
	@NotEmpty(message = "is required")
	private String lastName;
	@Column(name = "email")
	private String email;
	@Column(name = "driving_licence_number")
	@NotEmpty(message = "is required")
	private String drivingLicenceNumber;
	@Column(name = "aadhar_card_number")
	@Size(min = 16, max = 16, message = "must be a valid Aadhar Card Number")
	private String aadharCardNumber;
	@Column(name = "status", nullable = false, columnDefinition = "tinyint(1) default 1")
	private boolean status = true;
	@Lob
	@Column(name = "driving_licence", columnDefinition = "BLOB")
	private byte[] drivingLicence;
	@Lob
	@Column(name = "aadhar_card", columnDefinition = "BLOB")
	private byte[] aadharCard;
	@ManyToOne
	@JoinColumn(name = "tap_id")
	private Tap tap;
	@OneToOne
	@JoinColumn(name = "assigned_vehicle_number")
	private Vehicle assignedVehicleNumber;

	private @Transient boolean isNew = true;

	public Driver() {
		// TODO Auto-generated constructor stub
	}

	public Driver(String mobile, String firstName, String lastName, String email, String drivingLicenceNumber,
			String aadharCardNumber, boolean status) {
		this.mobile = mobile;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.drivingLicenceNumber = drivingLicenceNumber;
		this.aadharCardNumber = aadharCardNumber;
		this.status = status;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDrivingLicenceNumber() {
		return drivingLicenceNumber;
	}

	public void setDrivingLicenceNumber(String drivingLicenceNumber) {
		this.drivingLicenceNumber = drivingLicenceNumber;
	}

	public String getAadharCardNumber() {
		return aadharCardNumber;
	}

	public void setAadharCardNumber(String aadharCardNumber) {
		this.aadharCardNumber = aadharCardNumber;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public byte[] getDrivingLicence() {
		return drivingLicence;
	}

	public void setDrivingLicence(byte[] drivingLicence) {
		this.drivingLicence = drivingLicence;
	}

	public byte[] getAadharCard() {
		return aadharCard;
	}

	public void setAadharCard(byte[] aadharCard) {
		this.aadharCard = aadharCard;
	}

	public Tap getTap() {
		return tap;
	}

	public void setTap(Tap tap) {
		this.tap = tap;
	}

	public Vehicle getAssignedVehicleNumber() {
		return assignedVehicleNumber;
	}

	public void setAssignedVehicleNumber(Vehicle assignedVehicleNumber) {
		this.assignedVehicleNumber = assignedVehicleNumber;
	}

	@Override
	public String toString() {
		return "Driver [mobile=" + mobile + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
				+ ", drivingLicenceNumber=" + drivingLicenceNumber + ", aadharCardNumber=" + aadharCardNumber
				+ ", status=" + status + ", tap=" + tap + ", assignedVehicleNumber=" + assignedVehicleNumber + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mobile == null) ? 0 : mobile.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Driver other = (Driver) obj;
		if (mobile == null) {
			if (other.mobile != null)
				return false;
		} else if (!mobile.equals(other.mobile))
			return false;
		return true;
	}

	@Override
	public String getId() {
		return mobile;
	}

	@PrePersist
	@PostLoad
	void markNotNew() {
		this.isNew = isNew;
	}

	@Override
	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

}
