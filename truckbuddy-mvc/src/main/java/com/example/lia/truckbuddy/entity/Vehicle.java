package com.example.lia.truckbuddy.entity;

import java.util.Arrays;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;

import org.springframework.data.domain.Persistable;

@Entity
@Table(name = "vehicle")
public class Vehicle implements Persistable<String> {

	@Id
	@Column(name = "vehicle_number")
	@NotEmpty(message = "is required")
	private String vehicleNumber;
	@ManyToOne
	@JoinColumn(name = "tap_id")
	private Tap tap;
//	@OneToMany(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
//	@JoinColumn(name = "vehicle_types")
//	private Set<VehicleType> vehicleTypes = new HashSet<VehicleType>();

	@Column(name = "vehicle_types")
	@NotEmpty(message = "is required")
	private String vehicleTypes;
	@Column(name = "status")
	private boolean status = false;
	@Temporal(TemporalType.DATE)
	@Column(name = "insurance_expiry_date", columnDefinition = "DATE")
//	@FutureOrPresent(message = "must be a valid date")
	private Date insuranceExpiryDate;
	@Temporal(TemporalType.DATE)
	@Column(name = "permit_expiry_date", columnDefinition = "DATE")
//	@FutureOrPresent(message = "must be a valid date")
	private Date permitExpiryDate;
	@Lob
	@Column(name = "registration_certificate", columnDefinition = "longblob")
	private byte[] registrationCertificate;
	@OneToOne(mappedBy = "assignedVehicleNumber")
	private Driver driver;

	private @Transient boolean isNew = true;

	public Vehicle() {
		// TODO Auto-generated constructor stub
	}

	public Vehicle(String vehicleNumber, String vehicleTypes, boolean status) {
		this.vehicleNumber = vehicleNumber;
		this.vehicleTypes = vehicleTypes;
		this.status = status;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public Tap getTap() {
		return tap;
	}

	public void setTap(Tap tap) {
		this.tap = tap;
	}

//	public Set<VehicleType> getVehicleTypes() {
//		return vehicleTypes;
//	}
//
//	public void setVehicleTypes(Set<VehicleType> vehicleTypes) {
//		this.vehicleTypes = vehicleTypes;
//	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getVehicleTypes() {
		return vehicleTypes;
	}

	public void setVehicleTypes(String vehicleTypes) {
		this.vehicleTypes = vehicleTypes;
	}

	public Date getInsuranceExpiryDate() {
		return insuranceExpiryDate;
	}

	public void setInsuranceExpiryDate(Date insuranceExpiryDate) {
		this.insuranceExpiryDate = insuranceExpiryDate;
	}

	public Date getPermitExpiryDate() {
		return permitExpiryDate;
	}

	public void setPermitExpiryDate(Date permitExpiryDate) {
		this.permitExpiryDate = permitExpiryDate;
	}

	public byte[] getRegistrationCertificate() {
		return registrationCertificate;
	}

	public void setRegistrationCertificate(byte[] registrationCertificate) {
		this.registrationCertificate = registrationCertificate;
	}

	public Driver getDriver() {
		return driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	@Override
	public String toString() {
		return "Vehicle [vehicleNumber=" + vehicleNumber + ", tap=" + tap + ", status=" + status
				+ ", insuranceExpiryDate=" + insuranceExpiryDate + ", permitExpiryDate=" + permitExpiryDate
				+ ", registrationCertificate=" + Arrays.toString(registrationCertificate) + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((vehicleNumber == null) ? 0 : vehicleNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vehicle other = (Vehicle) obj;
		if (vehicleNumber == null) {
			if (other.vehicleNumber != null)
				return false;
		} else if (!vehicleNumber.equals(other.vehicleNumber))
			return false;
		return true;
	}

	@Override
	public String getId() {
		return vehicleNumber;
	}

	@PrePersist
	@PostLoad
	void markNotNew() {
		this.isNew = isNew;
	}

	@Override
	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

}
