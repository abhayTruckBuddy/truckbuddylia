package com.example.lia.truckbuddy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.example.lia.truckbuddy.entity.Driver;

public interface DriverRepository extends JpaRepository<Driver, String> {

	List<Driver> findByTapMobile(@Param("tapId") String tapId);
}
