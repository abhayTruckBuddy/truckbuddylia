package com.example.lia.truckbuddy.common;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class FileUploadHelper {

	public void uploadFile(byte[] bytes, String fileName) {
		File dir = new File(System.getProperty("user.dir") + "\\src\\main\\resources\\static\\uploads");
		try {
			if (!dir.exists())
				dir.mkdirs();

			File uploadFile = new File(
					dir.getAbsolutePath() + File.separator + fileName + RandomNumberGenerator.getRandomNumeric(5));
			BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(uploadFile));
			outputStream.write(bytes);
			outputStream.close();

		} catch (Exception e) {
			e.getMessage();
		}
	}

	public byte[] uploadFile(MultipartFile file) {
		byte[] bytes = null;
		try {
			bytes = file.getBytes();
		} catch (Exception e) {
			e.getMessage();
		}
		return bytes;
	}
}
