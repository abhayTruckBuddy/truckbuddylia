package com.example.lia.truckbuddy.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.lia.truckbuddy.entity.Vehicle;
import com.example.lia.truckbuddy.repository.VehicleRepository;
import com.example.lia.truckbuddy.service.VehicleService;

@RestController
@RequestMapping("/truckbuddy/lia/api/tap")
public class VehicleController {

	@Autowired
	private VehicleService vehicleService;

	@PostMapping("/addvehicle")
	public Vehicle createTap(@RequestBody Vehicle vehicle) {
		vehicleService.saveVehicle(vehicle);
		return vehicle;
	}

	@GetMapping("/vehicles")
	public List<Vehicle> getVehicles() {
		List<Vehicle> vehicles = vehicleService.getVehicle();
		return vehicles;
	}

	@GetMapping("/vehicles/{vehicleNumber}")
	public Vehicle getVehicle(@PathVariable String vehicleNumber) {
		Vehicle vehicle = vehicleService.getVehicle(vehicleNumber);
		if (vehicle != null) {
			return vehicle;
		} else {
			throw new RuntimeException("Driver not found with id : " + vehicleNumber);
		}
	}

	@GetMapping("/vehiclesByTapId")
	public List<Vehicle> getVehiclesByTapId(@RequestParam("tapId") String tapId) {
		List<Vehicle> vehicle = vehicleService.getVehiclesByTapId(tapId);
		if (vehicle != null) {
			return vehicle;
		} else {
			throw new RuntimeException("Vehicle(s) not found for Tap Id : " + tapId);

		}
	}
}
