package com.example.lia.truckbuddy.service;

import java.util.List;

import com.example.lia.truckbuddy.entity.Driver;

public interface DriverService {

	public void saveDriver(Driver driver);

	public Driver getDriver(String driverId);

	public List<Driver> getDrivers();

	public List<Driver> getDriversByTapId(String tapId);
}
