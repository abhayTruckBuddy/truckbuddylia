package com.example.lia.truckbuddy.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.example.lia.truckbuddy.common.FileUploadHelper;
import com.example.lia.truckbuddy.entity.Tap;
import com.example.lia.truckbuddy.entity.Vehicle;
import com.example.lia.truckbuddy.service.VehicleService;

@Controller
@RequestMapping("/truckbuddy/lia/mvc/tap")
public class VechicleMvcController {

	@Autowired
	private VehicleService vehicleService;
	@Autowired
	private FileUploadHelper fileUploadHelper;

	@RequestMapping("/vehicleRegistationForm")
	public String showVehicleRegistrationForm(Model model) {
		Vehicle vehicle = new Vehicle();
		model.addAttribute("vehicle", vehicle);
		return "vehicle-registration-form";
	}

	@PostMapping("/addVehicle")
	public String addVehicle(@Valid @ModelAttribute("vehicle") Vehicle vehicle, BindingResult bindingResult,
			@RequestParam("file1") MultipartFile registrationCertificate, Model model) {

		if (bindingResult.hasErrors()) {
			return "vehicle-registration-form";
		}
		try {
			vehicle.setRegistrationCertificate(fileUploadHelper.uploadFile(registrationCertificate));
			Tap tap = new Tap();
			tap.setMobile("9900000001");
			vehicle.setTap(tap);
			vehicleService.saveVehicle(vehicle);
			if (vehicle.getRegistrationCertificate() == null) {
				model.addAttribute("uploadError", true);
				return "vehicle-registration-form";
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			model.addAttribute("uploadError", true);
			return "vehicle-registration-form";
		}
		return "redirect:/truckbuddy/lia/mvc/tap/showVehicleList";
	}

	@GetMapping("/showVehicleList")
	public String getVehicles(Model model) {
		List<Vehicle> vehicleList = vehicleService.getVehicle();
		model.addAttribute("vehicleList", vehicleList);
		return "vehicle-list";
	}
	
	@GetMapping("/showVehicleDetails")
	public String showTapDetails(@RequestParam("vehicleNumber") String vehicleNumber, Model model) {
		Vehicle vehicle = vehicleService.getVehicle(vehicleNumber);
		model.addAttribute("vehicle", vehicle);
		return "vehicle-details";
	}

}
