package com.example.lia.truckbuddy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DefaultMvcController {

	@RequestMapping("/")
	public String showHomePage() {
		return "home-page";
	}

	@GetMapping("/comingsoon")
	public String getComingSoon() {
		return "coming-soon";
	}
	
	@GetMapping("/demoTable")
	public String showDemotable() {
		return "table-demo";
	}

}
