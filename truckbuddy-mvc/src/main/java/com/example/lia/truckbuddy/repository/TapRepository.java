package com.example.lia.truckbuddy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.lia.truckbuddy.entity.Tap;

public interface TapRepository extends JpaRepository<Tap, String> {

}
