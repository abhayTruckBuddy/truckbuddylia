package com.example.lia.truckbuddy.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.lia.truckbuddy.entity.Driver;
import com.example.lia.truckbuddy.entity.Vehicle;
import com.example.lia.truckbuddy.service.DriverService;

@RestController
@RequestMapping("/truckbuddy/lia/api/tap")
public class DriverController {

	@Autowired
	private DriverService driverService;

	@PostMapping("/adddriver")
	public Driver createDriver(@RequestBody Driver driver) {
		driverService.saveDriver(driver);
		return driver;
	}

	@GetMapping("/drivers")
	public List<Driver> getDrivers() {
		List<Driver> Drivers = driverService.getDrivers();
		return Drivers;
	}

	@GetMapping("/drivers/{driverId}")
	public Driver getDriver(@PathVariable String driverId) {
		Driver driver = driverService.getDriver(driverId);
		if (driver != null) {
			return driver;
		} else {
			throw new RuntimeException("Driver not found with id : " + driverId);
		}
	}

	@GetMapping("/driversByTapId")
	public List<Driver> getDriversByTapId(@RequestParam("tapId") String tapId) {
		List<Driver> drivers = driverService.getDriversByTapId(tapId);
		if (drivers != null) {
			return drivers;
		} else {
			throw new RuntimeException("Driver(s) not found for Tap Id : " + tapId);

		}
	}
}
